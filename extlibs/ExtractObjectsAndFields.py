import codecs
import json
import os


#--------Class (Node)---------------
class Node (object):
    def __init__(self):
        self.Source = ''
        self.relationship =  []

    def __str__(self):  
        Cadena =  self.Source + os.linesep
        for CurrentNodeRelation in self.relationship:
            Cadena = Cadena + '     ' + CurrentNodeRelation + os.linesep
        return str(Cadena)
#------------------------------------

#Add new node & properties
def Add_Node (listGraph, element, Properties, SourceElement):
    ElementGraph = Node()
    ElementGraph.Source =str(SourceElement)  
    for p in Properties:
        ElementGraph.relationship.append(str(p['name']))    
    listGraph.append(ElementGraph)

#Create text Output File
def Create_File (ListGraph, Path, Output):
    OutFile = codecs.open(Output,'w')
    for CurrentNode in ListGraph:
        OutFile.write(str(CurrentNode) + os.linesep)
    OutFile.close()

#**********************************************
InputFile = '/Users/rmercader/Documents/Projects/Wave Tooling/Amazon S3/ac-sdo-repo-dev/mfg/Analytics for Manufacturing.json'
OutputFile = '/Users/rmercader/Documents/Projects/Wave Tooling/Amazon S3/ac-sdo-repo-dev/mfg/Analytics for Manufacturing Objects and Fields.txt'
Path = os.getcwd()

print (InputFile)
print (OutputFile)
#***********************************************

#-------------------MAIN ----------------------

graph = []

try:
    try:
        #Open and read Json file
        with open (InputFile) as file:
            Nodes=json.load(file)
    except Exception as e:
        print ('File name or path are wrong')
  
    #Extract sfdcDigest & digest properties. 
    for CurrentNode in Nodes:
        if ((Nodes[CurrentNode]['action'] == 'sfdcDigest') or (Nodes[CurrentNode]['action'] == 'digest')):
            Add_Node (graph, CurrentNode, Nodes[CurrentNode]['parameters']['fields'], Nodes[CurrentNode]['parameters']['object'])

    Create_File(graph, Path , OutputFile )
    
except Exception as e:
    print ('ra with errors - ' + str(e))