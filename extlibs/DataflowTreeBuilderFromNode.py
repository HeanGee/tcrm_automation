import collections
import json


class Graph(object):
    def __init__(self):
        self.relationship = {}

    def __str__(self):
        return str(self.relationship)


def Add_Node(graph, element):
    graph.relationship.update({element: []})


def RelateNodes(graph, element1, element2):
    Relationship_Unilateral(graph, element1, element2)


def Relationship_Unilateral(graph, origin, destination):
    graph.relationship[origin].append(destination)


def LookForWay(graph, FirstElement, func, PassedElementsList=[]):
    try:
        if FirstElement in PassedElementsList:
            return
        func(FirstElement)
        PassedElementsList.append(FirstElement)
        for neighbor in graph.relationship[FirstElement]:
            LookForWay(graph, neighbor, func, PassedElementsList)
    except Exception as e:
        print('Transformation Node Not Found: ' + str(e))


def PrintGraph(element):
    NodesList.append(element)


def Create_JsonFile(jsonData):
    with open(OutputFile, 'w') as outfile:
        json.dump(jsonData, outfile, sort_keys=False, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    # This is the node from which we want to retrieve its tree
    EndNode = 'Register_Shifted_Dataset_ResourceAbsence3'
    # This is the path to the file that contains the dataflow definition in JSON format
    InputFile = 'json-files/PRPL - 12 Management Level Chain.json'
    # This is the path of the JSON file that will be generated and will contain the dataflow definition for the node
    # tree
    OutputFile = 'output/ResourceAbsence.json'

    # ***********************************************

    # -------------------MAIN ----------------------

    graph = Graph()
    NodesList = []

    try:
        try:
            with open(InputFile) as file:
                Nodes = json.load(file, object_pairs_hook=collections.OrderedDict)
        except Exception as e:
            print('Input file not found.')

        # Create nodes from Json input file
        for DtNode in Nodes:
            Add_Node(graph, DtNode)

        # Create relationship between the nodes
        for DrRelation in Nodes:
            if Nodes[DrRelation]['action'] == 'edgemart':
                RelateNodes(graph, DrRelation, DrRelation)
            else:
                if Nodes[DrRelation]['action'] == 'append':
                    for DrSource in Nodes[DrRelation]['parameters']['sources']:
                        RelateNodes(graph, DrRelation, DrSource)
                else:
                    if (Nodes[DrRelation]['action'] == 'augment') or (Nodes[DrRelation]['action'] == 'update'):
                        RelateNodes(graph, DrRelation, Nodes[DrRelation]['parameters']['left'])
                        RelateNodes(graph, DrRelation, Nodes[DrRelation]['parameters']['right'])
                    else:
                        if (Nodes[DrRelation]['action'] == 'sfdcDigest') or (Nodes[DrRelation]['action'] == 'digest'):
                            RelateNodes(graph, DrRelation, DrRelation)
                        else:
                            RelateNodes(graph, DrRelation, Nodes[DrRelation]['parameters']['source'])

        NodesList = []
        LookForWay(graph, EndNode, PrintGraph)

        # Iterate a copy of the dictionary, deleting the nodes haven't relationship with 'EndNode'
        Dictionary2 = Nodes.copy()
        for drN in Dictionary2:
            if not (drN in NodesList):
                Nodes.pop(drN)

        Create_JsonFile(Nodes)

    except Exception as e:
        print('ra with errors - ' + str(e))
