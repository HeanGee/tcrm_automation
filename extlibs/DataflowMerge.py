# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import json
import collections

#Create Json Output File
def Create_File (Nodes_Total, Output):
    try:
        with open(OutputFile, 'w') as outfile:
            json.dump(Nodes_Total, outfile, sort_keys = False, indent = 4,ensure_ascii = False)
    except Exception as e:
        raise e
  
#**************************************************
#InputFile_Start = '/Users/rmercader/Documents/Projects/EA Ops/Org62 Dataflows/Wave_Operation_Support_II_w_OJ.json'
#InputFile_End = '/Users/rmercader/Documents/Projects/EA Ops/UAT1 Dataflows/Current/Ohana_Journey.json'
InputFile_Start = '/Users/rmercader/Documents/Projects/Wave Tooling/Docs+Files/Register_FscDemoAccountSnapshot.json'
InputFile_End = '/Users/rmercader/Documents/Projects/Wave Tooling/Docs+Files/Register_FscDemoQuota.json'
#OutputFile = '/Users/rmercader/Documents/Projects/EA Ops/Org62 Dataflows/Wave_Operation_Support_II_w_OJ_w_AE.json'
OutputFile = '/Users/rmercader/Documents/Projects/Wave Tooling/Docs+Files/TimeShiftingDatasets.json'

#**************************************************

#-------------------MAIN ----------------------
try:
    #Read Json Input file A (InputFile_Start, Nodes_A)
    with open (InputFile_Start) as file:
            Nodes_A=json.load(file, object_pairs_hook=collections.OrderedDict)

    #Read Json Input file B (InputFile_End, Nodes_B)
    with open (InputFile_End) as file:
            Nodes_B=json.load(file, object_pairs_hook=collections.OrderedDict)

    Nodes_A_Copy = Nodes_A.copy()

    #Iterate a List of Nodes_A (Nodes_A_Copy) and verify if each node exist into the Nodes_B group.
    for CurrentNode in Nodes_A_Copy:
        if (CurrentNode in Nodes_B): 
           Nodes_A.pop(CurrentNode)  
    
    #Merge Nodos_B, Nodos_A
    Nodes_B.update(Nodes_A)

    #Generate output File
    Create_File(Nodes_B, OutputFile)
    
except Exception as e:
    print ('Executed with errors: ' + str(e))