import os
import sys
import re
from libs.utils import current_datetime


if __name__ == "__main__":
    today = current_datetime(add_time=False)

    branch_name = input("Branch name: ").strip()
    commit_msg = input("Commit Message: ").strip()

    if not re.match(fr"", branch_name):
        raise SystemError("Branch name invalid: The format must follow 'yy-mm-dd/arbitrary-text' syntax.")

    os.system(f"git checkout -b {branch_name}")
    os.system(f"git add {today}/*")
    os.system(f"git commit -m '{commit_msg}'")
    os.system("git checkout develop")
    os.system(f"export GIT_MERGE_AUTOEDIT=no")
    os.system(f"git merge --no-ff {branch_name}")
    os.system(f"git push origin develop {branch_name}")
    os.system("unset GIT_MERGE_AUTOEDIT")
