import json as js
import os
import re
import sys

from libs.deprecation_libs import delete_fields_of_deleted_node, perform_deprecation
from libs.json_libs import DEPRECATING_FIELDS_METADATA_FILENAME, get_nodes_by_action
from libs.utils import current_datetime, relative_mkdir
from libs.workbench_api_libs import get_dataflows_metadata
from libs.prompt_libs import progress_bar_with_time

if __name__ == '__main__':
    sys_parm = 0

    if '--use-line' in sys.argv:
        sys_parm = int(sys.argv[2]) + 1

    today = current_datetime(add_time=False)

    logs_dir = f"{today}/logs"
    output_dir = f"{today}/updated_dataflows"
    dataflows_dir = f"{today}/original_dataflows"
    fields_to_deprecate_md_file = f"{today}/{DEPRECATING_FIELDS_METADATA_FILENAME}"

    if not os.path.isdir(dataflows_dir):
        raise FileNotFoundError(f"Missing '{dataflows_dir}' directory "
                                f"which should contain the dataflow files to processs.")
    elif not os.path.isfile(fields_to_deprecate_md_file):
        raise FileNotFoundError(f"Missing '{fields_to_deprecate_md_file}' file in '{today}' directory.")
    elif not os.path.isdir(output_dir):
        relative_mkdir(name=output_dir, as_pymodule=False)

    if not os.path.isdir(logs_dir):
        relative_mkdir(name=logs_dir, as_pymodule=False)

    # Lists all the dataflow files and filter only json files just in case
    dataflows_names = os.listdir(dataflows_dir)
    dataflows_names = [name for name in dataflows_names if re.match(r'.*\.json', name)]
    # dataflows_names = ['Success Hub LATAM Dataflow.json']

    # Gets the field list metadata
    fieldlist_metadata = js.load(open(fields_to_deprecate_md_file))
    fieldlist = fieldlist_metadata.copy()

    env = input("Salesforce Instance Environment: ").strip()

    try:
        with progress_bar_with_time("Getting dataflows metadata from Salesforce", line=sys_parm, clear=False, last=False):
            dataflows, dataflow_ids, dataflow_names, dataflow_labels = get_dataflows_metadata(env=env,
                                                                                              print_instance=True)
    except Exception as e:
        print(e)
        env = None

    print('\n\n---------------------------------------------\n')
    for dataflow_name in dataflows_names:
        print(f" * Processing the dataflow '{dataflow_name}'...", end='\r')

        log_file = open(f"{logs_dir}/{dataflow_name}.log", 'w')

        # Reformat the original json file
        dataflow_file = open(f'{dataflows_dir}/{dataflow_name}', 'r')
        dataflow_json = js.load(dataflow_file)  # Loads the dataflow json
        dataflow_file.close()
        dataflow_file = open(f'{dataflows_dir}/{dataflow_name}', 'w')
        js.dump(dataflow_json, dataflow_file, indent=2)
        dataflow_file.truncate()
        dataflow_file.close()

        # Open dataflow for deprecation
        dataflow_file = open(f'{dataflows_dir}/{dataflow_name}')
        dataflow_json = js.load(dataflow_file)  # Loads the dataflow json

        try:
            node_list = get_nodes_by_action(df=dataflow_json, action=['sfdcDigest', 'digest', 'edgemart'])
            json_modified = perform_deprecation(df=dataflow_json, fieldlist=fieldlist,
                                                node_list=node_list, df_name=dataflow_name, log_file=log_file)
            json_modified = delete_fields_of_deleted_node(json_modified)

            try:
                if env:
                    df_id = dataflow_ids[dataflow_names.index(dataflow_name.replace('.json', ''))]
                    dataflow_output_name = f"{output_dir}/id.{df_id}.name.{dataflow_name}"
                else:
                    raise ValueError("Salesforce Environment Not Specified.")
            except Exception as e:
                print(f"\n    {e}")
                print(f"    Using default name for the output file: '{dataflow_name}'")
                dataflow_output_name = f"{output_dir}/{dataflow_name}"

            with open(dataflow_output_name, 'w') as log_file:
                js.dump(json_modified, log_file, indent=2)

            print(f" √ Dataflow '{dataflow_name}' processing finished. [OK]")
            print()
        except Exception as e:
            print()
            log_file.close()
            dataflow_file.close()
            raise e
        finally:
            log_file.close()
            dataflow_file.close()

    print('---------------------------------------------\n')
    print(f" All the logs for each dataflow can be found at '{logs_dir}'\n")
