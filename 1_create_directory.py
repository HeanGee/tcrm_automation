import json as js

from libs.json_libs import DATAFLOW_LIST_FILENAME, DEPRECATING_FIELDS_METADATA_FILENAME
from libs.utils import current_datetime, relative_mkdir

today = current_datetime(add_time=False)
relative_mkdir(today, as_pymodule=False)

folders = ["original_dataflows", "updated_dataflows", "customer_files"]
_ = [relative_mkdir(name=f"{today}/{_dir}", as_pymodule=False) for _dir in folders]

fieldlist_metadata = [
    {
        "object": "<< Salesforce Object Name >>",
        "status": "active",
        "source-node": "<< For now, just ignore it and set as empty string >>",
        "fields": ["<< Name of the field to deprecate 1 >>", "<< Name of the field to deprecate 2 >>", "<< etc >>"],
        "defaults": {
            "digest": ["x", "x", "x"],
            "sfdcDigest": ["x", "x", "x"],
            "edgemart": ["x", "x", "x"],
            "augment": ["x", "x", "x"],
            "append": ["x", "x", "x"],
            "update": ["x", "x", "x"],
            "filter": ["x", "x", "x"],
            "dim2mea": ["x", "x", "x"],
            "computeExpression": ["x", "x", "x"],
            "computeRelative": ["x", "x", "x"],
            "flatten": ["x", "x", "x"],
            "sliceDataset": ["x", "x", "x"],
            "prediction": ["x", "x", "x"],
            "sfdcRegister": ["x", "x", "x"]
        }
    }
]

with open(f"{today}/{DEPRECATING_FIELDS_METADATA_FILENAME}", 'w') as f:
    js.dump(fieldlist_metadata, indent=2, fp=f)

with open(f"{today}/{DATAFLOW_LIST_FILENAME}", 'w') as f:
    f.writelines(['02KZ00000003C38MAE\n', 'Wave_Business_Medium\n', 'MAP - Assessments\n'])

template_field_files = ["Fieldlist_Account.txt", "Fieldlist_Company__c.txt", "Fieldlist_sfbase__OpportunityTeam__c.txt",
                        "Fieldlist_Case.txt", "Fieldlist_Opportunity.txt"]
for tff in template_field_files:
    open(f"{today}/{tff}", 'w').close()

print(f"Directory '{today}' created.")
print(f"Directory '{today}/original_dataflows' created.")
print(f"Directory '{today}/updated_dataflows' created.")
print(f"Dataflow list file '{today}/{DATAFLOW_LIST_FILENAME}' created.")
print(f"Template file '{today}/{DEPRECATING_FIELDS_METADATA_FILENAME}' created.")
print()
