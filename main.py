import os

if __name__ == '__main__':
    while True:
        os.system('clear')

        title = "Field deprecator Script."

        print(title)
        print('-' * len(title), end='\n\n')

        options_msg = [
            ('Create today\'s folder structure.', "python 1_create_directory.py"),
            ('Create metadata file for the fields to delete..', "python 2_create_fields_metadata.py"),
            ('Download dataflows.', "python 3_pull_dataflows.py"),
            ('Execute field deprecation.', "python deprecate_fields.py"),
            ('Upload the dataflows.', "python 6_upload_dataflows.py"),
        ]

        print('Select an option:')

        menu_idx = 1
        for msg, _ in options_msg:
            print(f"\t{menu_idx})  {msg}")
            menu_idx += 1
        print(f"\n\t0) Exit from script.")

        option = input('> ').strip()

        if not option.isdigit():
            print('   - Invalid option')
            input('Press ENTER to continue...')
            continue

        option = int(option) - 1

        if option == -1:
            print("Exit from script.")
            break

        if option not in range(len(options_msg)):
            print('\n>> invalid option <<\n')

        _, command = options_msg[option]

        if option in [2, 3]:
            os.system(f'{command} --use-line {8 + len(options_msg)}')
        else:
            os.system(command)

        input('Press ENTER to continue...')
