from libs.json_libs import DATAFLOW_LIST_FILENAME
from libs.prompt_libs import progress_bar_with_time
from libs.workbench_api_libs import download_dataflow, validate_dataflow
from libs.utils import current_datetime
import os
import sys
from threading import Thread

if __name__ == '__main__':
    line = 0
    try:
        today = current_datetime(add_time=False)

        if not os.path.isdir(today):
            raise NotADirectoryError(f"Missing required folder: '{today}'.")

        dataflow_list_filename = f"{today}/{DATAFLOW_LIST_FILENAME}"

        if not os.path.isfile(dataflow_list_filename):
            raise FileNotFoundError(f"Missing the required file: '{dataflow_list_filename}'")

        dataflow_list_file = open(dataflow_list_filename, 'r')

        dataflow_list = [
            line.strip().replace("\n", "")
            for line in dataflow_list_file.readlines() if line not in ['', '\n']
        ]

        if not len(dataflow_list):
            raise FileNotFoundError(f"No dataflow name was found in the file '{dataflow_list_filename}'.")

        if '--use-line' in sys.argv:
            line = int(sys.argv[2]) + 1

        env = input("Salesforce Instance Environment: ").strip()
        if env not in ['Sandbox', 'Production'] and env in ['', '\n']:
            env = "Sandbox"
            print(">> Environment set to 'Sandbox' by default <<")

        with progress_bar_with_time(prefix="Validating the dataflows", line=line, clear=False, last=False):
            validate_dataflow(df_name=dataflow_list, env=env)

        threads = []
        line += 1
        print_instance = True

        with progress_bar_with_time(prefix="Downloading the dataflows", line=line, clear=False, last=False):
            for dataflow in dataflow_list:
                th = Thread(target=download_dataflow,
                            kwargs={'df_name': dataflow, 'env': env, 'print_instance': print_instance})
                threads.append(th)
                th.start()

                print_instance = False

            for th in threads:
                th.join()
        print('\n' * 3)
    except Exception as e:
        print('\n' * (line + 1))
        raise e
