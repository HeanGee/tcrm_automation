import json as js
import os
from os import path, system
from pathlib import Path
from typing import List

import yaml as ym

from .libs.json_libs import node_is
from .libs.utils import current_datetime


def expand_home(path):
    if path:
        home = str(Path.home())
        path = path.replace('~', home)
        path = path.replace('~', home)

    return path


def tree_extractor(dataflow: dict, registers: List[str], output_dir: str, output_filename: str):
    if not output_filename:
        output_filename = "Extracted Dataflow.json"

    # Auxiliary variables
    not_processed_parents = registers.copy() if registers else []
    detected_parents = []
    node_using_source = ['sfdcRegister', 'filter', 'dim2mea', 'computeExpression', 'computeRelative',
                         'sliceDataset', 'prediction', 'flatten']

    while len(not_processed_parents):
        node = not_processed_parents.pop()
        if node_is(action=node_using_source, node=dataflow[node]):
            parent = dataflow[node]['parameters']['source']
            not_processed_parents.append(parent)
        elif node_is(action=['augment', 'update'], node=dataflow[node]):
            parent_left = dataflow[node]['parameters']['left']
            parent_right = dataflow[node]['parameters']['right']
            not_processed_parents.append(parent_left)
            not_processed_parents.append(parent_right)
        elif node_is(action='append', node=dataflow[node]):
            sources = dataflow[node]['parameters']['sources']
            not_processed_parents += sources

        detected_parents.append(node)

    detected_parents = list(set(detected_parents))

    # Saves the subree in a file
    today = current_datetime(add_time=False)
    subtree = {idx: elm for idx, elm in dataflow.items() if idx in detected_parents}

    while True:
        if len(detected_parents) and path.isdir(os.path.join('libs/tcrm_automation', today)):
            extraction_folder = output_dir

            if not os.path.isdir(extraction_folder):
                os.mkdir(extraction_folder)

            if path.isdir(extraction_folder):
                filename = f"{extraction_folder}/{output_filename}{'.json' if output_filename[-5:] != '.json' else ''}"

                with open(filename, 'w') as f:
                    js.dump(subtree, f, indent=2)

                print(f"\nNew file generated at '{filename}'")

            else:
                raise NotADirectoryError(f"It wasn't able to create the folder '{extraction_folder}'.")

            break

        elif not path.isdir(os.path.join('libs/tcrm_automation', today)):
            print(today)
            os.system('pwd')
            option = input(f"Missing '{today}' folder. Create?")

            if option in ['y', 'Y', 'yes', 'YES', 'Yes', 'si', 'SI', 'si']:
                os.makedirs(os.path.join('libs/tcrm_automation', today))
            else:
                break

        elif not len(detected_parents):
            print(f"\nNo tree detected.")
            break


if __name__ == '__main__':
    # Source: https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/

    # YAML File Format:
    # ================
    # dataflow: json2.json
    # output: Wave Operation Support II Extracted
    # registers:
    #   - Compute_Attendee_Account_Owner_Role_Segment
    #   - Register_Attendee_SIC_EMEA
    #   - Register_EBC_Meeting_SIC_EMEA

    system('clear')

    with open('tree_extractor_md.yml') as md_file:
        extractor_md = ym.load(md_file, Loader=ym.SafeLoader)

        df_filename = expand_home(extractor_md['dataflow'])
        output_fn = expand_home(extractor_md['output'])
        registers = [register.rstrip() for register in extractor_md['registers']]

        dataflow = js.load(open(df_filename, 'r'))
        today = current_datetime(add_time=False)

        tree_extractor(dataflow=dataflow, registers=registers, output_dir=today, output_filename=output_fn)
