import json as js
import yaml as ym

from libs.tcrm_automation.libs.json_libs import node_is, get_nodes_by_action
from os import system
from pathlib import Path
from typing import List


def expand_home(path):
    home = str(Path.home())
    path = path.replace('~', home)
    path = path.replace('~', home)

    return path


def tree_remover(dataflow: dict, replacers: List[dict], registers: List[str], output: str):
    system('clear')

    not_processed_parents = registers.copy() if registers else []  # initially, all registers.
    detected_parents = []  # It will collects all ancestry nodes of those registers.

    # Auxiliary variables
    node_using_source = ['sfdcRegister', 'filter', 'dim2mea', 'computeExpression', 'computeRelative', 'flatten',
                         'sliceDataset', 'prediction']

    while len(not_processed_parents):
        node = not_processed_parents.pop()
        if node in [None, ""]:
            continue

        if node in ["", None, '']:
            continue

        if node_is(action=node_using_source, node=dataflow[node]):
            parent = dataflow[node]['parameters']['source']
            not_processed_parents.append(parent)

        elif node_is(action=['augment', 'update'], node=dataflow[node]):
            parent_left = dataflow[node]['parameters']['left']
            parent_right = dataflow[node]['parameters']['right']
            not_processed_parents.append(parent_left)
            not_processed_parents.append(parent_right)

        elif node_is(action='append', node=dataflow[node]):
            sources = dataflow[node]['parameters']['sources']
            not_processed_parents += sources

        detected_parents.append(node)

    detected_parents = list(set(detected_parents))

    # Merges all replacers into one
    big_replacer = {}
    for replacer in replacers:
        common_node = set(big_replacer.keys()).intersection(set(replacer.keys()))
        if common_node:
            raise KeyError(f"Replacers have {len(common_node)} common node{'s' if len(common_node) > 1 else ''}: "
                           f"[{', '.join(list(common_node))}]")
        big_replacer.update(replacer)

    # ================== Merge digests ==================
    main_digests = get_nodes_by_action(df=dataflow, action="sfdcDigest")
    reps_digests = get_nodes_by_action(df=big_replacer, action="sfdcDigest")
    main_keys = [nodename for (nodename, _) in main_digests]
    reps_keys = [nodename for (nodename, _) in reps_digests]
    common_digests = set(main_keys).intersection(reps_keys)
    if common_digests:
        common_digests = list(common_digests)
        for key in common_digests:
            main_fields = dataflow[key]['parameters']['fields']
            reps_fields = big_replacer[key]['parameters']['fields']
            delta = [field for field in reps_fields if field not in main_fields]
            if delta:
                big_replacer[key]['parameters']['fields'] = main_fields + delta
    # ================== Merge digests ==================

    # Updates the dataflow
    dataflow.update(big_replacer)

    # Deletes all detected parent not in replacer or
    # Deletes all detected parent if no replacer was specified.
    for node in detected_parents:
        if not big_replacer:
            del dataflow[node]
        else:
            if node not in big_replacer.keys():
                del dataflow[node]

    msg = "Removed the following nodes:"
    detected_parents.sort()
    print(f"{msg}\n{'=' * len(msg)}")
    print("\n".join(detected_parents))
    print("=" * len(msg))

    if big_replacer:
        new_nodes = list(big_replacer.keys())
        msg = "\nAdded new nodes:"
        new_nodes.sort()
        print(f"{msg}\n{'=' * len(msg)}")
        print("\n".join(new_nodes))
        print("=" * len(msg))

    with open(output, 'w') as _f:
        js.dump(dataflow, _f, indent=2)

    print(f"\nNew file generated at '{output}'")


if __name__ == '__main__':
    # Source: https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/

    with open('tree_remover_md.yml') as leaf_remover_md_file:
        leaf_remover_md = ym.load(leaf_remover_md_file, Loader=ym.SafeLoader)

        dataflow_filename = expand_home(leaf_remover_md['dataflow'])
        replacer_df_filename = leaf_remover_md['replacer']
        register_nodes = leaf_remover_md['registers']
        output = expand_home(leaf_remover_md['output'])

        print(f"Processing {dataflow_filename}:\n")

        with open(dataflow_filename, 'r') as f:
            df = js.load(f)

            tree_remover(dataflow=df,
                         replacers=[js.load(open(expand_home(rep_file), 'r')) for rep_file in replacer_df_filename],
                         registers=register_nodes, output=output)
