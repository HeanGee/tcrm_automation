import json as js
import os
import re

from libs.json_libs import DEPRECATING_FIELDS_METADATA_FILENAME
from libs.utils import current_datetime

if __name__ == "__main__":
    today = current_datetime(add_time=False)

    print(f"Checking for the folder '{today}'...", end='\r')
    if not os.path.isdir(today):
        raise NotADirectoryError(f"Necessary directory not found: '{today}'")
    print(f"Checking for the folder '{today}'... [ok]")

    static_filename_part = "Fieldlist_"
    _, _, filenames = next(os.walk(today))
    filenames = [f for f in filenames if re.search(fr"^{static_filename_part}[\w\d_]+\.txt", f)]

    print(f"Checking if the fieldlist file exists...", end='\r')
    if not len(filenames):
        raise FileNotFoundError("No fieldlist file was found.")
    print(f"Checking if the fieldlist file exists... [ok]")

    fieldlist_md = []

    for filename in filenames:
        search_result = re.search(fr"^{static_filename_part}(.*?)\.txt", filename)

        print(f"   - Processing the file '{filename}'...", end='\r')
        if not search_result:
            msg = f"The file '{filename}' doesn't have the expected format: " \
                  f"Must be '{static_filename_part}<org62 object name>.txt'"
            raise NameError(msg)

        object_name = search_result.group(1)

        with open(f"{today}/{filename}") as f:
            fieldlist = [x.strip().replace('\n', '') for x in f.readlines() if x not in ["", "\n"]]

        if not len(fieldlist):
            raise ValueError(f"The file '{filename}' is empty. It must have at least one field to deprecate.")

        defaults = ['x' for _ in fieldlist]
        fieldlist_md.append({
            "object": object_name,
            "status": "active",
            "source-node": "",
            "fields": fieldlist,
            "defaults": {
                "digest": defaults,
                "sfdcDigest": defaults,
                "edgemart": defaults,
                "augment": defaults,
                "append": defaults,
                "update": defaults,
                "filter": defaults,
                "dim2mea": defaults,
                "computeExpression": defaults,
                "computeRelative": defaults,
                "flatten": defaults,
                "sliceDataset": defaults,
                "prediction": defaults,
                "sfdcRegister": defaults
            }
        })
        print(f"   √ Processing the file '{filename}'... [ok]")

    fieldlist_md_filepath = f"{today}/{DEPRECATING_FIELDS_METADATA_FILENAME}"
    js.dump(obj=fieldlist_md, fp=open(fieldlist_md_filepath, 'w'), indent=2)

    print()
    if not os.path.isfile(fieldlist_md_filepath):
        msg = f"File '{fieldlist_md_filepath}' wasn't be created. Check for system resource or the path permission."
        raise IOError(msg)
    else:
        print(f"File '{fieldlist_md_filepath}' was updated successfully.")
