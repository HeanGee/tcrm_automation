import logging
import os
from datetime import datetime
from pathlib import Path

import yaml
import requests
import tzlocal

from .connection_libs import authenticate

LOGOUT_ENDPOINT = {
    "Sandbox": "https://test.salesforce.com/services/oauth2/revoke",
    "Production": "https://login.salesforce.com/services/oauth2/revoke"
}

CONNECTION_CACHE_FILE = "connection_cache.yml"


class Logger(object):
    TAB = 0
    LOG_FILE = None

    def __init__(self):
        super(Logger, self).__init__()

    @classmethod
    def up(cls):
        cls.TAB += 1

    @classmethod
    def down(cls):
        cls.TAB -= 1

    @classmethod
    def log(cls, msg, log_file):
        logging.basicConfig(filename=log_file.name, filemode='a',
                            format='%(levelname)s:%(asctime)s:   %(message)s',
                            datefmt='%m-%d-%y %H:%M:%S',
                            level=logging.DEBUG,
                            force=cls.LOG_FILE != log_file.name)

        if log_file.name != cls.LOG_FILE:
            cls.LOG_FILE = log_file.name

        # if msg.strip():
        logging.info(f"{'|  ' * cls.TAB}{msg}")


def current_datetime(add_time=False):
    utc = tzlocal.get_localzone()
    return datetime.now(tz=utc).strftime(f"%Y-%m-%d{' %H.%M.%S%z' if add_time else ''}")


def relative_mkdir(name="", as_pymodule=True):
    if name:
        Path(name).mkdir(parents=True, exist_ok=True)
        if as_pymodule:
            init_file_path = f"{os.getcwd()}/{name}/__init__.py"
            if not (os.path.isfile(init_file_path) or os.path.isdir(init_file_path)):
                open(init_file_path, 'w').close()


def connect(env: str = "Sandbox", refresh: bool = False):
    if not refresh and os.path.exists(CONNECTION_CACHE_FILE):
        with open(CONNECTION_CACHE_FILE, 'r') as cache_file:
            _conn_cache = yaml.load(cache_file, Loader=yaml.SafeLoader)

            response = {"instance_url": _conn_cache['instance_url']}
            header = _conn_cache['header']
    else:
        key, sec, username, password = get_connapp_credential_from_env()

        response = authenticate(env, username, password, key, sec)
        header = {'Authorization': "Bearer " + response["access_token"], 'Content-Type': "application/json"}

        with open(CONNECTION_CACHE_FILE, 'w') as cache_file:
            yaml.dump({"instance_url": response['instance_url'], 'header': header}, cache_file)

    return response, header


def disconnect(env: str, token: str):
    logout_endpoint = LOGOUT_ENDPOINT[env]
    response = requests.post(logout_endpoint, data={"token": token})

    print(response.text)
    
    if os.path.exists(CONNECTION_CACHE_FILE):
        os.remove(CONNECTION_CACHE_FILE)

    return response


def get_connapp_credential_from_env():
    consumer_key_name = "CONNECTED_APP_CONSUMER_KEY"
    consumer_sec_name = "CONNECTED_APP_CONSUMER_SECRET"
    username_name = "CONNECTED_APP_USERNAME"
    password_name = "CONNECTED_APP_PASSWORD"
    required_env_keys = [consumer_key_name, consumer_sec_name, username_name, password_name]

    sys_env_keys = list(os.environ)

    if not set(required_env_keys).issubset(sys_env_keys):
        required_env_keys = [rek for rek in required_env_keys if rek not in sys_env_keys]
        msg = f"Missing required env vars: [{', '.join(required_env_keys)}]"
        raise EnvironmentError(msg)

    key = os.getenv(consumer_key_name)
    sec = os.getenv(consumer_sec_name)
    username = os.getenv(username_name)
    password = os.getenv(password_name)

    return key, sec, username, password
