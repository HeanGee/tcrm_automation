from copy import deepcopy
from typing import Union

try:
    try:
        from libs.exception_collection import InvalidActionException, InvalidNodeRequestException
    except ImportError as e:
        from ..libs.exception_collection import InvalidActionException, InvalidNodeRequestException
except ImportError as e:
    from libs.tcrm_automation.libs.exception_collection import InvalidActionException, InvalidNodeRequestException

DIGEST_KEYS = ['sfdcDigest', 'digest']
REGISTER_KEYS = ['sfdcRegister']
VALID_NODE_ACTIONS = ['sfdcDigest', 'digest', 'edgemart', 'augment', 'filter', 'sfdcRegister', 'dim2mea',
                      'computeRelative', 'computeExpression', 'prediction', 'flatten', 'sliceDataset', 'append',
                      'update']

FIELD_REF_REGEX = r'[a-zA-Z_]+'
COMPOSED_FIELD_REF_REGEX = r'([\']([a-zA-Z_0-9]+\.)*(<field>)[\'])'
"""
To use the `COMPOSED_FIELD_REF_REGEX`:
COMPOSED_FIELD_REF_REGEX.replace('<field>', FIELD_REF_REGEX)
"""
PROCESED_NODES = []
DELETED_NODES = []
RNODES_WITH_REPLACED_FIELD = {}
FNODES_WITH_REPLACED_FIELD = {}

DEPRECATING_FIELDS_METADATA_FILENAME = "fields_to_deprecate_md.json"
DATAFLOW_LIST_FILENAME = "dataflow_list.txt"


def valid_action(action: Union[str, list]):
    """
    Checks if the `action` is valid.

    :param action: The `action` to check.
    :return: True if `action` is valid otherwise False.
    """

    if isinstance(action, list):
        return all([act in VALID_NODE_ACTIONS for act in action])

    return action in VALID_NODE_ACTIONS


def node_is(action: Union[str, list], node: dict):
    """
    Checks if the action property of a given `node` is `action`.

    :param action: The `action` asked for.
    :param node: The noded being checked.
    :return: True if the `node` action is `action` otherwise False.
    """

    if isinstance(action, list):
        return node['action'] in action

    return node['action'] == action


def delete_node(nodename: str = None, df: dict = None, fieldlist: list = None, action: str = ""):
    """
    Deletes the given node from the json and uptates the childs parent to match the parent of the deleted node.

    :param nodename: The name of the `node` to delete.
    :param df: The json data.
    :param fieldlist: The container of the fields to delete. This container will be updated dynamically being added all
    the composed fields and computed fields from `augment` or `computeExpression`/`computeRelative` nodes.
    :param action: The action of the node to delete.
    :return: The list of all children of the deleted node.
    """

    if nodename not in df.keys():
        return

    # Updates the source of the childs to match the source of this node,
    # If the node to delete is an 'augment' or 'update',
    #  their child's sources have to match the 'left' source of this node.
    node_to_delete = df[nodename]
    childs, _ = get_child_nodes(node_name=nodename, df=df, mark_left_childs=True)

    if not any([node_is(act, node=node_to_delete) for act in ['augment', 'update', 'append']]):
        current_node_source = df[nodename]['parameters']['source']
        df = set_source(df=df, old_source_name=nodename, source_node_name=current_node_source, child_nodes=childs)
    elif any([node_is(action=act, node=node_to_delete) for act in ['augment', 'update']]):
        left_source_nm = node_to_delete['parameters']['left']
        df = set_source(df=df, old_source_name=nodename, source_node_name=left_source_nm, child_nodes=childs)

    # Builds an entry in fieldlist to indicate the field to delete: The old object the field belong will be deleted
    entry = None
    source_node = nodename

    if node_is(action='dim2mea', node=node_to_delete):
        field_to_delete = node_to_delete['parameters']['measure']
        default_value = ['x']
        fields_to_delete = []

        for fieldmd in fieldlist:
            if field_to_delete not in fieldmd['fields']:
                continue

            idx = fieldmd['fields'].index(field_to_delete)
            replacement = fieldmd['defaults'][action][idx]

            if replacement == "x":
                continue

            fields_to_delete.append(fieldmd['defaults'][action][idx])

        if len(fields_to_delete):
            entry = {
                "object": "",
                "status": "active",
                "source-node": source_node,
                "fields": field_to_delete,
                "defaults": {
                    "digest": default_value,
                    "sfdcDigest": default_value,
                    "edgemart": default_value,
                    "augment": default_value,
                    "filter": default_value,
                    "dim2mea": default_value,
                    "computeExpression": default_value,
                    "computeRelative": default_value,
                    "flatten": default_value,
                    "prediction": default_value,
                    "sfdcRegister": default_value
                }
            }

    if entry:
        fieldlist.append(entry)

    # Deletes the node
    del df[nodename]

    return childs


def set_source(df: dict, old_source_name: str, source_node_name: str, child_nodes: list):
    """
    Updates the source of the nodes received with the specified new source name.

    :param df: The json data.
    :param old_source_name: The old source name to be replaced.
    :param source_node_name: The new source name to be replaced by.
    :param child_nodes: The list of child nodes
    :return: The updated json data.
    """

    for child_nm, child in child_nodes:
        if not (node_is('augment', child) or node_is('append', child) or node_is('update', child)) and \
                child['parameters']['source'] == old_source_name:
            child['parameters']['source'] = source_node_name

        elif node_is('augment', child):
            if child['parameters']['left'] == old_source_name:
                backup_old_left = child['parameters']['left']

                child['parameters']['left'] = source_node_name
                child['parameters']['old_left'] = backup_old_left  # Used in deletion

            elif child['parameters']['right'] == old_source_name:
                backup_old_right = child['parameters']['right']
                child['parameters']['right'] = source_node_name

                if 'old_right' not in child['parameters'].keys():
                    df[child_nm]['parameters']['old_right'] = ""
                df[child_nm]['parameters']['old_right'] = backup_old_right

        elif node_is('update', child):
            # If the node to delete is a left of right source of an update, the update node itself becomes useless.
            # Therefore, the update node can be deleted and its child's sources have to match the source of the
            #  node to be deleted.
            if old_source_name in [child['parameters']['left'], child['parameters']['right']]:
                childs, _ = get_child_nodes(node_name=child_nm, df=df)
                df = set_source(df=df, old_source_name=child_nm, source_node_name=source_node_name, child_nodes=childs)
                del df[child_nm]

        # If we assume that the source of all `append` nodes is always a `digest` node,
        #  unless a `digest` node is deleteable, this code will never run.
        elif node_is('append', child):
            if old_source_name in child['parameters']['sources']:
                backup_sources = child['parameters']['sources'].copy()
                sources = backup_sources.copy()

                sources = [source_node_name if s == old_source_name else s for s in sources]

                if len(sources) < 2:
                    childs, _ = get_child_nodes(node_name=child_nm, df=df)
                    df = set_source(df=df, old_source_name=child_nm,
                                    source_node_name=source_node_name, child_nodes=childs)
                    del df[child_nm]
                else:
                    df[child_nm]['parameters']['sources'] = sources

    return df


def get_nodes_by_action(df: {} = None, action: Union[str, list] = ''):
    """
    Returns a list of the shape:

    [('key1', json_object_1), ('key2', json_object_2), ...]

    :param df: The json-object dataflow json.
    :param action: A string indicating the type of action.
    :return: A List of json-object representing the located nodes.
    """

    if isinstance(action, list):
        for act in action:
            if not valid_action(act):
                raise InvalidActionException(f"Invalid action '{act}' from the list [{', '.join(action)}].")
    else:
        if not valid_action(action):
            raise InvalidActionException(f"Invalid action: '{action}'.")

    nodes = []

    if not (df and action):
        return nodes

    for node_name in df:
        if 'action' in df[node_name].keys() and (
                isinstance(action, list) and df[node_name]['action'] in action or
                isinstance(action, str) and df[node_name]['action'] == action):
            nodes.append((node_name, df[node_name]))

    return nodes


def get_child_nodes(node_name: str = "", df: {} = None, mark_left_childs: bool = False):
    """
    Returns a list of tuples (node_name, ndde). The node represents all the child node of `node_name` node.

    :param mark_left_childs: Whether to mark the childs that are left_source of the `node_name` node.
    :param node_name: The name of the node considered as root.
    :param df: The main dataflow json.
    :return: List of tuples representing the child nodes.
    """

    if node_name not in df.keys():
        raise InvalidNodeRequestException(f"{{{node_name}}} node doesn't exist.")

    _df = df

    augment = get_nodes_by_action(df=_df, action='augment')
    update = get_nodes_by_action(df=_df, action='update')
    append = get_nodes_by_action(df=_df, action='append')
    filters = get_nodes_by_action(df=_df, action='filter')
    dim2Mea = get_nodes_by_action(df=_df, action='dim2mea')
    express = get_nodes_by_action(df=_df, action='computeExpression')
    relativ = get_nodes_by_action(df=_df, action='computeRelative')
    flattrn = get_nodes_by_action(df=_df, action='flatten')
    sliceds = get_nodes_by_action(df=_df, action='sliceDataset')
    predict = get_nodes_by_action(df=_df, action='prediction')
    registr = get_nodes_by_action(df=_df, action='sfdcRegister')

    r_sourced_augments = [(nodename, node) for nodename, node in augment if node['parameters']['right'] == node_name]

    for name, rsource in r_sourced_augments:
        if 'child_by_left' in rsource['parameters'].keys():
            del df[name]['parameters']['child_by_left']

    l_sourced_augments = [(nodename, node) for nodename, node in augment if node['parameters']['left'] == node_name]

    if mark_left_childs:
        for _, lsource in l_sourced_augments:
            lsource['parameters']['child_by_left'] = 1

    equal_parent_detected = False
    for i in range(len(l_sourced_augments)):
        for j in range(len(r_sourced_augments)):
            lnm, lnd = l_sourced_augments[i]
            rnm, rnd = r_sourced_augments[j]

            if lnm == rnm:
                _rnd = deepcopy(rnd)
                if 'child_by_left' in _rnd['parameters'].keys():
                    del _rnd['parameters']['child_by_left']
                    r_sourced_augments[j] = (rnm, _rnd)
                    equal_parent_detected = True
            if equal_parent_detected:
                break
        if equal_parent_detected:
            break

    childs = l_sourced_augments
    childs += r_sourced_augments

    childs += [(nodename, node) for nodename, node in update
               if node_name in [node['parameters']['left'], node['parameters']['right']]] + \
              [(nodename, node) for nodename, node in append if node_name in node['parameters']['sources']] + \
              [(nodename, node) for nodename, node in filters if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in dim2Mea if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in express if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in relativ if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in flattrn if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in sliceds if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in predict if node['parameters']['source'] == node_name] + \
              [(nodename, node) for nodename, node in registr if node['parameters']['source'] == node_name]

    collected_nodes = []
    _childs = []

    for key, node in childs:
        # if key not in collected_nodes:
        if key not in collected_nodes or node_is('augment',
                                                 node):  # 'update' output the same whether it comes as left or right
            # child
            _childs.append((key, node))
            collected_nodes.append(key)

    return _childs, [name for name, _ in l_sourced_augments]
