class InvalidActionException(Exception):
    """Exception raised when an invalid action is requested."""
    pass


class InvalidNodeRequestException(Exception):
    """Exception raised when an invalid action node is requested."""
    pass


class LastFieldDeletionException(Exception):
    """Exception raised when the field to delete is the only one from a given node."""
    pass


class DigestNodeEmptyException(Exception):
    """Exception raised when the field to delete is the last one from a digest node."""
    pass


class KeyFieldDeletionException(Exception):
    """Exception raised when the field to delete is a key (left or right) of an augment/update node."""
    pass


class FilterNodeFieldMissingReplacementException(Exception):
    """Exception raised when the field to delete is a key (left or right) of an augment node."""
    pass


class RelativeFieldDeletionException(Exception):
    """Exception raised when the field to delete one of ['partitionBy', 'orderBy'] field of a computeRelative node."""
    pass


class DefaultValueNotFoundException(Exception):
    """Exception raised when the input file doesn't specify a default value for the field to delete."""
    pass

