import csv
import html.parser as HTMLParser
import json
import traceback

import requests


def makeGetRequest(URL, header):
    response = {}
    try:
        response = requests.get(URL, headers=header)
    except Exception as e:
        print("Encountered Exception" + str(e))
        return None

    response_code = str(response.status_code)

    if response.status_code != 200:
        raise ConnectionError(f"Connection failed. Reason: {response.text}")

    if response_code[:1] != "2":
        print(response_code)
        return None
    if response.text:
        response = response.json()

    return response


def make_patch_request(url, data, header):
    try:
        response = requests.patch(url, json=data, headers=header)
    except Exception as e:
        print("Encountered Exception" + str(e))
        return None

    response_code = str(response.status_code)

    if response.status_code != 200:
        raise ConnectionError(f"Connection failed. Reason: {response.text}")

    if response_code[0] != '2':
        print(response_code)
        print(url)
        return None

    if response.text:
        response = response.json()

    return response, response_code


def makePostRequest(URL, header, data):
    response = {}
    try:
        response = requests.post(URL, headers=header, data=data)
    except Exception as e:
        print("Encountered Exception" + str(e))
        return None

    response_code = str(response.status_code)

    if response_code[0] != '2':
        print(response_code)
        print(URL)
        return None

    if response.text:
        response = response.json()

    return response


def authenticate(env, username, password, client_id, client_secret):
    valid_envs = ['Sandbox', 'Production']

    if env not in valid_envs:
        raise KeyError(f"The Environment '{env}' specified is not valid. It must to be 'Sandbox' or 'Production'.")

    authURL = {"Sandbox": "https://test.salesforce.com/services/oauth2/token",
               "Production": "https://login.salesforce.com/services/oauth2/token"}

    grant_type = "password"

    authenticationData = {'grant_type': grant_type,
                          'client_id': client_id,
                          'client_secret': client_secret,
                          'username': username,
                          'password': password}

    response_json = makePostRequest(authURL[env], {}, authenticationData)

    if response_json is None:
        raise ConnectionError("Authentication Failed: Review your credentials for the connected app.")

    return response_json


def getDatasetJson(datasetAPIName):
    URL = data['instance_url'] + "/services/data/v49.0/wave/datasets/" + datasetAPIName

    dataset = makeGetRequest(URL, header)

    if dataset is None:
        print("Dataset Not found " + datasetAPIName)
        return None

    if ("currentVersionUrl" in dataset):
        URL = data['instance_url'] + dataset["currentVersionUrl"]
    else:
        print("Current Version of ", datasetAPIName, " dataset is not Available")
        return None

    dataset = makeGetRequest(URL, header)

    return dataset


def getinfo(field_type, name, totalFieldNameList, json_file):
    fieldlist = []
    if ("xmdMain" in json_file and field_type in json_file["xmdMain"]):
        for obj in json_file["xmdMain"][field_type]:
            temp = [name, obj["field"], field_type]
            fieldlist.append(temp)
            totalFieldNameList.append(obj["field"])
    else:
        print("xmdMain not found or field_type not found in ", name)
    return fieldlist


def getFieldsUsageInfo(dependency_type, json_file, totalFieldNameList, FieldsUsageList, Datasetname):
    if json_file is None:
        print("Json is Empty")
        return None

    if (dependency_type in json_file):
        json_file = json_file[dependency_type]["dependencies"]
        for obj in json_file:
            URL = data['instance_url'] + obj["url"]
            dependency_type_json = makeGetRequest(URL, header)

            if (dependency_type_json is None):
                print("Error while fetching json of ", dependency_type)
                continue
            json_String = str(dependency_type_json)
            for key in totalFieldNameList:

                if (json_String.find(key) != -1):
                    assetInfo = {"URL": dependency_type_json["assetSharingUrl"], "type": dependency_type,
                                 "label": dependency_type_json["label"]}
                    FieldsUsageList[key]["FieldUsage"][dependency_type_json["label"]] = assetInfo
                    FieldsUsageList[key]["count"] += 1

        return FieldsUsageList
    else:
        print("This ", Datasetname, " Dataset is not used used any ", dependency_type)
        return None


def getPredicateUsage(json_file, FieldsUsageList, str_predicate, totalFieldNameList, name):
    if (str_predicate is None):
        return None

    if json_file is None:
        print("json of Current Version of ", name, " dataset does not exist")
        return None

    for key in totalFieldNameList:
        if (str_predicate.find(key) != -1):
            assetInfo = {"URL": json_file["dataset"]["url"], "type": "SecurityPredicate", "label": name}
            FieldsUsageList[key]["FieldUsage"][name + "_SecurityPredicate"] = assetInfo
            FieldsUsageList[key]["count"] += 1

    return FieldsUsageList


def getID(dataset_name):
    URL = data['instance_url'] + "/services/data/v49.0/wave/datasets/" + dataset_name
    dataset = makeGetRequest(URL, header)

    print(dataset["id"])
    return dataset["id"]


def getDependenciesJson(asset_id):
    URL = data['instance_url'] + "/services/data/v49.0/wave/dependencies/" + asset_id
    dependency = makeGetRequest(URL, header)

    if (dependency is None):
        print("Request Failed while fetching dependency")
    return dependency


def mergeJsonFields(datasetJson):
    if ("xmdMain" in datasetJson):
        datasetJson = datasetJson["xmdMain"]
        if ("dates" in datasetJson):
            datasetJson = datasetJson["dates"]
            datesSubFields = []
            FullFields = []
            for Objs in datasetJson:

                subFieldsList = [Objs["fields"]["fullField"]]
                FullFields.append(Objs["fields"]["fullField"])
                for subFields in Objs["fields"]:
                    subFieldsList.append(Objs["fields"][subFields])
                datesSubFields.append(subFieldsList)

        else:
            print("No Dates")
            return None
        return [FullFields, datesSubFields]
    else:
        print("No xmdMain")
        return None


def cleantotalFieldNameList(totalFieldNameList, json_file):
    MergeList = mergeJsonFields(json_file)
    if (MergeList == None):
        return totalFieldNameList
    num = 0
    MergeFullFields = MergeList[0]
    datesSubFields = MergeList[1]

    while (num < len(datesSubFields)):
        for i in range(1, len(datesSubFields[num])):
            if (datesSubFields[num][i] not in MergeFullFields and datesSubFields[num][i] in totalFieldNameList):
                totalFieldNameList.remove(datesSubFields[num][i])
        num += 1

    return totalFieldNameList


def getFieldDetails(json_file, name):
    if json_file is None:
        print("json of Current Version of ", name, " dataset does not exist")
        return None
    totalFieldNameList = []
    dimensions = getinfo("dimensions", name, totalFieldNameList, json_file)
    measures = getinfo("measures", name, totalFieldNameList, json_file)
    totalFieldNameList = cleantotalFieldNameList(totalFieldNameList, json_file)

    finallist = [dimensions, measures]

    dependency_JSON = getDependenciesJson(json_file["dataset"]["id"])
    FieldsUsageList = {}
    for obj in totalFieldNameList:
        FieldsUsageList[obj] = {}
        FieldsUsageList[obj]["count"] = 0
        FieldsUsageList[obj]["FieldUsage"] = {}
    securityPredicateUsage = {}

    if ("predicate" in json_file):
        str_predicate = json_file["predicate"]
        securityPredicateUsage = getPredicateUsage(json_file, FieldsUsageList, str_predicate, totalFieldNameList, name)

    DashboardFieldsDict = getFieldsUsageInfo("dashboards", dependency_JSON, totalFieldNameList, FieldsUsageList, name)
    LensFieldsDict = getFieldsUsageInfo("lenses", dependency_JSON, totalFieldNameList, FieldsUsageList, name)

    return FieldsUsageList


def getNumOfFieldsUsed(currentDatasetFieldUsage):
    NumOfFieldsUsed = 0
    for obj in currentDatasetFieldUsage:
        if (currentDatasetFieldUsage[obj]["count"] != 0):
            NumOfFieldsUsed += 1
    return NumOfFieldsUsed


def getDatasetAliasList(dataflowName):
    dataflow = open(dataflowName, "r")
    dataflow_json = json.load(dataflow)

    dataset_alias_list = []

    for node in dataflow_json:
        if dataflow_json[node]["action"] == "sfdcRegister":
            if dataflow_json[node]["parameters"]["alias"] not in dataset_alias_list:
                dataset_alias_list.append(dataflow_json[node]["parameters"]["alias"])

    return dataset_alias_list


def getDataflowFieldsUsage(DataflowName):
    dataset_alias_list = getDatasetAliasList(DataflowName)
    if (dataset_alias_list is None):
        print("Found no datasets in this ", DataflowName, " Dataflow")
        return None

    dataflowFieldsUsage = {}
    FieldWiseDetails = {}

    for dataset_alias in dataset_alias_list:
        dataflowFieldsUsage[dataset_alias] = {}
        dataflowFieldsUsage[dataset_alias]["FieldWiseDetails"] = {}
        dataflowFieldsUsage[dataset_alias]["noOfFieldsUsed"] = 0
        dataflowFieldsUsage[dataset_alias]["noOfFieldsUnUsed"] = 0

    for dataset_alias in dataset_alias_list:
        datasetJson = getDatasetJson(dataset_alias)
        if (datasetJson == None):
            print("Error in fetching dataset JSON")
            continue
        currentDatasetFieldUsage = getFieldDetails(datasetJson, dataset_alias)
        dataflowFieldsUsage[dataset_alias]["FieldWiseDetails"] = currentDatasetFieldUsage
        if currentDatasetFieldUsage is None:
            continue

        NumOfFieldsUsed = getNumOfFieldsUsed(currentDatasetFieldUsage)
        dataflowFieldsUsage[dataset_alias]["noOfFieldsUsed"] = NumOfFieldsUsed
        dataflowFieldsUsage[dataset_alias]["noOfFieldsUnUsed"] = len(currentDatasetFieldUsage) - NumOfFieldsUsed

    jsonOutput = open("FinalDataflowFieldsUsage.json", "w")
    dataflowFieldsUsage
    jsonOutput.write(json.dumps(dataflowFieldsUsage))
    jsonOutput.close()

    print("Successfull!")

    String = HTMLParser.unescape(dataflowFieldsUsage)
    return dataflowFieldsUsage


def CSV_Convert(dataflowFieldsUsage, dataFlowName, csvType):
    FieldInfoList = []
    AssetInfoList = []

    if (dataflowFieldsUsage is None):
        return None

    if (csvType == "FieldsUsageInfo"):
        Headers_List = ["DataflowName", "DatasetName", "FieldName", "FieldUsage", "IsUsed"]
        FieldInfoList.append(Headers_List)
    else:
        Headers_List = ["DataflowName", "DatasetName", "FieldName", "AssetLabel", "AssetType", "AssetURL"]
        AssetInfoList.append(Headers_List)

    for dataset in dataflowFieldsUsage:
        if (dataset is None):
            continue
        for Fields in dataflowFieldsUsage[dataset]["FieldWiseDetails"]:
            IsUsed = "True"
            if (dataflowFieldsUsage[dataset]["FieldWiseDetails"][Fields]["count"] == 0):
                IsUsed = "False"
            tempFields = [dataFlowName, dataset, Fields,
                          dataflowFieldsUsage[dataset]["FieldWiseDetails"][Fields]["count"], IsUsed]
            FieldInfoList.append(tempFields)
            for Assets in dataflowFieldsUsage[dataset]["FieldWiseDetails"][Fields]["FieldUsage"]:
                tempAssets = [dataFlowName, dataset, Fields,
                              dataflowFieldsUsage[dataset]["FieldWiseDetails"][Fields]["FieldUsage"][Assets]["label"]
                    , dataflowFieldsUsage[dataset]["FieldWiseDetails"][Fields]["FieldUsage"][Assets]["type"],
                              dataflowFieldsUsage[dataset]["FieldWiseDetails"][Fields]["FieldUsage"][Assets]["URL"], ]
                AssetInfoList.append(tempAssets)
    if (csvType == "FieldsUsageInfo"):
        return FieldInfoList
    else:
        return AssetInfoList


def csv_files(dataFlowName):
    with open('FieldUsage_CSV.csv', 'w', encoding='utf8') as FieldUsage_CSV:
        csv_writer = csv.writer(FieldUsage_CSV)
        resultList = CSV_Convert(dataflowFieldsUsage, dataFlowName, "FieldsUsageInfo")
        if (resultList is not None):
            csv_writer.writerows(resultList)
        else:
            print("Empty FieldsUsageInfo")

    with open('Assets_CSV.csv', 'w', encoding='utf8') as Assets_CSV:
        csv_writer = csv.writer(Assets_CSV)
        resultList = CSV_Convert(dataflowFieldsUsage, dataFlowName, "AssetsUsageInfo")
        if (resultList is not None):
            csv_writer.writerows(resultList)
        else:
            print("Empty AssetsUsageInfo")


def getDataflowUtilizationFiles(DataflowName, Environment, UserName, Password, client_id, client_secret):
    global data, header
    try:
        data = authenticate(Environment, UserName, Password, client_id, client_secret)
        if (data is None):
            print("Authentication Failed")
        header = {'Authorization': "Bearer " + data["access_token"], 'Content-Type': "application/json"}
        global dataflowFieldsUsage
        dataflowFieldsUsage = {}
        dataflowFieldsUsage = getDataflowFieldsUsage(DataflowName + '.json')
        if (dataflowFieldsUsage is None):
            print("Error while retrieving Fields Usage Data of ", DataflowName, " Dataflow")
        csv_files(DataflowName)  # comment this line if you don't want csv files to be created
        return dataflowFieldsUsage
    except Exception as e:
        print("Exception :", e)
        track = traceback.format_exc()
        print(track)
        print("Sorry! An Exception occurred")


if __name__ == "__main__":
    dataflowName = "InputDataflow"  # do not include file extension
    env = "Sandbox"  # (Sandbox | Production)
    user = "dmin@salesforce.com.devpro61"
    password = "8ABHYFkjNzmhR"
    client_id = "3MVG9GiqKapCZBwEtNyEQ0U2Pv34k4ziXjebvIMgh7mW2jGmX6h9ZIls_K9gMU0CFz_6kw5HcvNpE7kV5QFeo"
    client_secret = "27A8FA1974441479425E8372A3F8A0D6F2F10F55F9EF62B92E430D049A238E2E"

    resultDict = getDataflowUtilizationFiles(dataflowName, env, user, password, client_id, client_secret)  # creates csv
    # files and dependency json, upload csv into einstein analytics

    '''
    creates 3 files:
    FinalDataflowFieldUsage.json - contains entire dependency info in a json format
    FieldUsage_CSV - contains Dataset-Field usage info (used/notUsed)
    AssetUsage_CSV - contains information about where exactly the fields are used
    The CSV files can be uploaded into Einstein to create a dashboard
    '''

    print(resultDict)  # dependency json
