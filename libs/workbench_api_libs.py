from typing import Union

from libs.connection_libs import makeGetRequest, make_patch_request
from libs.utils import connect, current_datetime
import json as js
import os
import re


DATAFLOWS_URL = {
    "Sandbox": '/services/data/v51.0/wave/dataflows/',
    "Production": '/services/data/v50.0/wave/dataflows/',
}


def get_dataflows_metadata(env: str = "Sandbox", print_instance=False):
    try:
        response, header = connect(env=env)

        instance_url = response['instance_url']

        if print_instance:
            print(f"Instance: {instance_url}")

        dataflows_url = instance_url + DATAFLOWS_URL[env]

        response = makeGetRequest(dataflows_url, header)
    except ConnectionError as conn_err:
        print(conn_err)

        response, header = connect(env=env, refresh=True)

        instance_url = response['instance_url']

        if print_instance:
            print(f"Instance: {instance_url}")

        dataflows_url = instance_url + DATAFLOWS_URL[env]

        response = makeGetRequest(dataflows_url, header)

    dataflows = response['dataflows']

    dataflow_ids = [df['id'] for df in dataflows]
    dataflow_names = [df['name'] for df in dataflows]
    dataflow_labels = [df['label'] for df in dataflows]

    return dataflows, dataflow_ids, dataflow_names, dataflow_labels


def validate_dataflow(df_name: Union[str, list] = "", env: str = "Sandbox"):
    dataflows, dataflow_ids, dataflow_names, dataflow_labels = get_dataflows_metadata(env=env, print_instance=True)

    if isinstance(df_name, str):
        if not (df_name in dataflow_ids or df_name in dataflow_names or df_name in dataflow_labels):
            raise FileNotFoundError(f"'{df_name}' dataflow doesn't exist.")
    elif isinstance(df_name, list):
        for dfn in df_name:
            if not(dfn in dataflow_ids or dfn in dataflow_names or dfn in dataflow_labels):
                raise FileNotFoundError(f"'{dfn}' dataflow doesn't exist.")

    return True


def get_dataflow_id(df_name: str = "", env: str = "Sandbox"):
    dataflows, dataflow_ids, dataflow_names, dataflow_labels = get_dataflows_metadata(env=env)

    index = -1
    if df_name in dataflow_ids:
        index = dataflow_ids.index(df_name)
    elif df_name in dataflow_names:
        index = dataflow_names.index(df_name)
    elif df_name in dataflow_labels:
        index = dataflow_labels.index(df_name)

    return dataflow_ids[index]


def get_dataflow_name(df_name: str = "", env: str = "Sandbox"):
    dataflows, dataflow_ids, dataflow_names, dataflow_labels = get_dataflows_metadata(env=env)

    index = -1
    if df_name in dataflow_ids:
        index = dataflow_ids.index(df_name)
    elif df_name in dataflow_names:
        index = dataflow_names.index(df_name)
    elif df_name in dataflow_labels:
        index = dataflow_labels.index(df_name)

    return dataflow_names[index]


def download_dataflow(df_name: str = "", env: str = "", print_instance: bool = False):
    today = current_datetime(add_time=False)
    dataflows, dataflow_ids, dataflow_names, dataflow_labels = get_dataflows_metadata(env=env,
                                                                                      print_instance=print_instance)

    try:
        response, header = connect(env=env)

        instance_url = response['instance_url']

        df_id = get_dataflow_id(df_name=df_name, env=env)
        df_name = get_dataflow_name(df_name=df_name, env=env)

        dataflow_metadata_get_url = instance_url + DATAFLOWS_URL[env] + df_id

        dataflow_definition = makeGetRequest(dataflow_metadata_get_url, header)['definition']
    except ConnectionError as conn_err:
        print(conn_err)

        response, header = connect(env=env, refresh=True)

        instance_url = response['instance_url']

        df_id = get_dataflow_id(df_name=df_name, env=env)
        df_name = get_dataflow_name(df_name=df_name, env=env)

        dataflow_metadata_get_url = instance_url + DATAFLOWS_URL[env] + df_id

        dataflow_definition = makeGetRequest(dataflow_metadata_get_url, header)['definition']

    with open(f"{today}/original_dataflows/{df_name}.json", 'w') as f,\
            open(f"{today}/updated_dataflows/id.{df_id}.name.{df_name}.json", 'w') as g:
        js.dump(dataflow_definition, f, indent=2)
        js.dump(dataflow_definition, g, indent=2)


def upload_dataflow(df_path: str = "", env: str = "Sandbox"):
    print(f"\n* Verifying today directory...", end='\r')
    if not os.path.isdir(df_path):
        raise NotADirectoryError(f"Directory '{df_path}' doesn't exist.")
    print(f"√ Verifying today directory... [ok]")

    if env not in ['Sandbox', 'Production'] and env in ['', '\n']:
        env = "Sandbox"
        print(">> Environment set to 'Sandbox' by default <<")

    dataflows_names = os.listdir(df_path)
    dataflows_names = [name for name in dataflows_names if re.match(r'.*\.json', name)]

    print(f"\n* Verifying amount of dataflows to upload...", end='\r')
    if not len(dataflows_names):
        raise FileNotFoundError(f"No dataflow json file to upload found at '{df_path}' directory.")
    print(f"√ Verifying amount of dataflows to upload... [ok]")

    try:
        response, header = connect(env=env)

        instance_url = response['instance_url']

        msg = "Proceeding to upload dataflows..."
        print(f"\n{msg}\n{'-' * len(msg)}", end='\n')
        for dfn in dataflows_names:
            nm_components = dfn.split('.')

            if len(nm_components) != 5:
                raise NameError(f"The modified dataflow file '{dfn}' doesn't have the expected format.")

            df_id = nm_components[1]

            dflow_patch_url = instance_url + DATAFLOWS_URL[env] + df_id

            dflow_file = open(f"{df_path}/{dfn}", 'r')

            dflow_definition = {
                "definition": js.load(fp=dflow_file)
            }

            print(f"  * Uploading dataflow '{dfn}'...", end='\r')
            response, response_code = make_patch_request(url=dflow_patch_url, data=dflow_definition, header=header)
            if response_code == '200':
                print(f"  √ Uploading dataflow '{dfn}'... [ok]")
                print(f"    √ Response status code: {response_code}")
            else:
                msg = "Someting went wrong with the upload process. Contact to the project admin to review it."
                raise ConnectionError(msg)
    except ConnectionError as conn_err:
        print(conn_err)

        response, header = connect(env=env, refresh=True)

        instance_url = response['instance_url']

        msg = "Proceeding to upload dataflows..."
        print(f"\n{msg}\n{'-' * len(msg)}", end='\n')
        for dfn in dataflows_names:
            nm_components = dfn.split('.')

            if len(nm_components) != 5:
                raise NameError(f"The modified dataflow file '{dfn}' doesn't have the expected format.")

            df_id = nm_components[1]

            dflow_patch_url = instance_url + DATAFLOWS_URL[env] + df_id

            dflow_file = open(f"{df_path}/{dfn}", 'r')

            dflow_definition = {
                "definition": js.load(fp=dflow_file)
            }

            print(f"  * Uploading dataflow '{dfn}'...", end='\r')
            response, response_code = make_patch_request(url=dflow_patch_url, data=dflow_definition, header=header)
            if response_code == '200':
                print(f"  √ Uploading dataflow '{dfn}'... [ok]")
                print(f"    √ Response status code: {response_code}")
            else:
                msg = "Someting went wrong with the upload process. Contact to the project admin to review it."
                raise ConnectionError(msg)

    print()