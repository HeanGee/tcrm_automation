import copy
import re
from typing import Union

try:
    from libs.exception_collection import DefaultValueNotFoundException, DigestNodeEmptyException, \
        KeyFieldDeletionException, LastFieldDeletionException, RelativeFieldDeletionException
    from libs.json_libs import RNODES_WITH_REPLACED_FIELD, delete_node, get_child_nodes, get_nodes_by_action
    from libs.utils import Logger
except ImportError as e:
    from libs.tcrm_automation.libs.exception_collection import DefaultValueNotFoundException, DigestNodeEmptyException, \
        KeyFieldDeletionException, LastFieldDeletionException, RelativeFieldDeletionException
    from libs.tcrm_automation.libs.json_libs import RNODES_WITH_REPLACED_FIELD, delete_node, get_child_nodes, get_nodes_by_action
    from libs.tcrm_automation.libs.utils import Logger


class Collection:
    def __init__(self):
        self.collection = None
        self.affected_nodes = None
        self.added_nodes = None
        self.original_df = None
        self.summary = None

        self.reset()

    def reset(self):
        self.collection: dict = {"sample-key": ['sample-value']}
        self.affected_nodes = []  # [(nodename1, node object1), (nodename2, node object2), ...]
        self.added_nodes = []  # [nodename1, nodename2, ...]
        self.original_df = None
        self.summary = {}

    def add(self, key: str = "", value: str = ""):
        if key and key not in self.collection.keys():
            self.collection[key] = []

        if value and value not in self.collection[key]:
            self.collection[key].append(value)

    def add_node(self, key: str, value: dict):
        if key not in self.added_nodes:
            self.affected_nodes.append((key, value))
            self.added_nodes.append(key)

    def get(self):
        _collection = copy.deepcopy(self.collection)
        return _collection

    def get_affected_nodes(self):
        _affected_nodes = copy.deepcopy(self.affected_nodes)
        return _affected_nodes

    def remove_temp(self):
        if 'sample-key' in self.collection.keys():
            del self.collection['sample-key']

    def set_original_df(self, original_df: dict = None):
        self.original_df = original_df

    def update_summary(self, nodename: str, object_name: str, field: str):
        registers = get_registers(nodes=[(nodename, self.original_df[nodename])], df=self.original_df)

        for register_nd_name in registers.keys():
            if register_nd_name not in self.summary.keys():
                self.summary[register_nd_name] = registers[register_nd_name]

            if object_name not in self.summary[register_nd_name].keys():
                self.summary[register_nd_name][object_name] = []

            if field not in self.summary[register_nd_name][object_name]:
                self.summary[register_nd_name][object_name].append(field)

    def get_summary(self):
        return self.summary


def get_registers(nodes: list, df: dict):
    registers = {}
    i = 0
    while len(nodes):
        (nodename, node) = nodes.pop(i)

        if node['action'] == 'sfdcRegister' and nodename not in registers.keys():
            registers[nodename] = {
                'dataset-name': node['parameters']['name'],
                'dataset-alias': node['parameters']['alias']
            }

        childs, _ = get_child_nodes(node_name=nodename, df=df)
        nodes += childs

    return registers


collection = Collection()


def perform_deprecation(df: dict = None, fieldlist: Union[dict, list] = None, node_list: list = None,
                        df_name: str = "", log_file=None, original_df: dict = None):
    """
    This function is the entry point to start the field deprecation.
    It process only the digest nodes (including 'digest', sfdcDigest' and 'edemart')

    :param df: The json data.
    :param fieldlist: The list of fields to deprecate and their replacement metadatas.
    :param node_list: The list of digest nodes.
    :param df_name: The name of the dataflow file the customer had specified.
    :param log_file: The output stream the script will output the log registry to.
    :return: The updated json data.
    """

    collection.reset()
    collection.set_original_df(original_df)

    for fieldmd in fieldlist:
        if fieldmd['status'] != 'active':
            continue

        # print("Now the fieldlist is", fieldlist)
        _fieldlist = [fieldmd.copy()]

        _delete_from_dataflow(df=df, fieldlist=_fieldlist, node_list=node_list,
                              df_name=df_name, log_file=log_file)

    collection.remove_temp()

    return df, collection


def _delete_from_dataflow(df: dict = None, fieldlist: Union[dict, list] = None, node_list: list = None,
                          df_name: str = "", parent_node: str = "", log_file=None):
    """
    The function receives a list of nodes to start the field deprecation process.

    :param df: The json data.
    :param fieldlist: The list of fields to deprecate and their replacement metadatas. Now it is a single element list.
    :param node_list: The list of nodes to process.
    :param df_name: The name of the dataflow specified by user.
    :return: The updated json data.
    """

    for nodename, node in node_list:
        # Check if the node exists in the dataflow: May had been deleted by previous process
        if nodename not in df.keys() or node['action'] in ['sfdcRegister']:
            continue

        # Collects first the children
        childs, _ = get_child_nodes(node_name=nodename, df=df)

        # Perform the deletion for current node
        # Logger.log('\n', log_file=log_file)
        Logger.log(f"node: {nodename}", log_file=log_file)
        Logger.log(f"{'-' * (len(nodename) + 6)}", log_file=log_file)
        Logger.log(f"childs: {',  '.join([name for name, _ in childs])}", log_file=log_file)
        delete_from_node(df=df, action=node['action'], nodemd=(nodename, node), fieldlist=fieldlist,
                         log_file=log_file, df_name=df_name, parent_node=parent_node)

        if Logger.TAB == 0:
            Logger.log("", log_file=log_file)

        if node['action'] in ['digest', 'sfdcDigest'] and 'object_not_correspond' in node['parameters'].keys():
            object_not_correspond = df[nodename]['parameters']['object_not_correspond']

            del df[nodename]['parameters']['object_not_correspond']

            if object_not_correspond == 1:
                continue

    return df


def delete_from_node(df: dict = None, action: str = '', nodemd=None, fieldlist: list = None, log_file=None,
                     df_name: str = "", parent_node: str = ""):
    """
    Performs the field deprecation (deletion or substitution) from a given node.

    :param parent_node: The name of the parent node.
    :param log_file: The log file which the process will output the log message to.
    :param df: The json data.
    :param action: The action of the node to process.
    :param nodemd: A tuple representing the node name and the node.
    :param fieldlist: The list of fields to deprecate and their replacements metadata.
    :param df_name: The json data.
    """

    if not len(nodemd):
        return

    nodename, node = nodemd
    composed_field_list_from_augment = []
    childs_of_deleted = None
    field_deleted_from_right_select = None

    for field_metadata in fieldlist:  # md stands for 'metadata'
        if field_metadata['status'] != 'active':
            continue

        obj_name = field_metadata['object']
        fields_to_del = field_metadata['fields']
        field_default = field_metadata['defaults'][action]
        source_node = field_metadata['source-node']
        do_replace = len(fields_to_del) == len(field_default)
        source_node_type = field_metadata['source-node-type'] if 'source-node-type' in field_metadata.keys() else None
        Logger.log(f"Fields to deprecate: {',  '.join(fields_to_del)}", log_file=log_file)

        if not do_replace:
            msg = f"Missing to specify default values in the input json for the action '{action}'."
            raise DefaultValueNotFoundException(msg)
        elif nodename not in df.keys():
            continue

        if action == "edgemart":
            if obj_name == 'delete-from-edgemart' and source_node == nodename:
                Logger.log(f"Skipping the edgemart node '{nodename}' and continue analysis with its children."
                           f" Proceeding its children.",
                           log_file=log_file)
                break

            if 'object_not_correspond' not in node['parameters'].keys():
                node['parameters']['object_not_correspond'] = 1
                break

        if action in ['digest', 'sfdcDigest']:
            if 'object_not_correspond' not in node['parameters'].keys():
                node['parameters']['object_not_correspond'] = 1

            if node['parameters']['object'] == obj_name:
                for field in fields_to_del:
                    # Deletes the digested fields or replace if corresponds
                    count_fields = len(node['parameters']['fields'])
                    new_field = field_default[fields_to_del.index(field)]

                    for i in range(count_fields):
                        if node['parameters']['fields'][i]['name'] == field:
                            node['parameters']['object_not_correspond'] = 0
                            if new_field != 'x':
                                df[nodename]['parameters']['fields'][i]['name'] = new_field
                                Logger.log(f"Replaced '{field}' by '{new_field}'", log_file=log_file)
                            else:
                                Logger.log(f"Removed the field '{df[nodename]['parameters']['fields'][i]['name']}'",
                                           log_file=log_file)
                                del df[nodename]['parameters']['fields'][i]

                                # Verifies if the final list of digested fields is empty, then raise exception
                                if not len(df[nodename]['parameters']['fields']):
                                    msg = f"Can not delete the field(s) [{', '.join(fields_to_del)}]" \
                                          f" of <code>{obj_name}</code> object from {{{nodename}}} digest node. " \
                                          f"This node can't have an empty list of fields."
                                    raise DigestNodeEmptyException(msg)

                                collection.add(obj_name, field)
                                collection.add_node(key=nodename, value=copy.deepcopy(node))
                                collection.update_summary(nodename, obj_name, field)

                        if i + 1 >= len(node['parameters']['fields']):
                            break

                    # Deletes or substitutes from `filterConditions`
                    if 'filterConditions' in node['parameters'].keys():
                        count_filter_cond = len(node['parameters']['filterConditions'])
                        for i in range(count_filter_cond):
                            extra_level = 'conditions' in node['parameters']['filterConditions'][i].keys()

                            if not extra_level:
                                if node['parameters']['filterConditions'][i]['field'] == field:
                                    if new_field != 'x':
                                        df[nodename]['parameters']['filterConditions'][i]['field'] = new_field
                                        Logger.log(f"FilterCondition {i} updated.", log_file=log_file)
                                    else:
                                        # No need to worry when there are no filterConditions (empty filterConditions)
                                        Logger.log(f"FilterCondition {i} removed.", log_file=log_file)
                                        del df[nodename]['parameters']['filterConditions'][i]
                            else:
                                for j in range(len(node['parameters']['filterConditions'][i]['conditions'])):
                                    if node['parameters']['filterConditions'][i]['conditions'][j]['field'] == field:
                                        df[nodename]['parameters']['filterConditions'][i]['conditions'][j]['field'] = new_field
                                        Logger.log(f"FilterCondition {i} updated.", log_file=log_file)

                            if i + 1 >= len(node['parameters']['filterConditions']):
                                break

                    # Deletes or substitutes from `complexFilterConditions`
                    if 'complexFilterConditions' in node['parameters'].keys():
                        cmplx_filter_cond = node['parameters']['complexFilterConditions']

                        if field in cmplx_filter_cond:
                            if new_field != "x":
                                cmplx_filter_cond = cmplx_filter_cond.replace(field, new_field)
                            else:
                                cmplx_filter_cond = ""

                        node['parameters']['complexFilterConditions'] = cmplx_filter_cond

        elif action == 'augment':
            child_by_left = 'child_by_left' in node['parameters'].keys()
            old_left = 'old_left' in node['parameters'].keys()

            if child_by_left or old_left and node['parameters']['old_left'] == parent_node:
                for field in fields_to_del:
                    if field in node['parameters']['left_key']:
                        msg = f"Can not delete the field '<strong><code>{field}</strong></code>' of <code>{obj_name}</code> object from <strong><code>{{{nodename}}}</strong></code> node. " \
                              f"The field appears as a <code>left_key</code> in an agument node. " \
                              f"For now, we don't support key field deletion from an augment node."
                        raise KeyFieldDeletionException(msg)

                Logger.log(f"Skipping aug node '{nodename}': Is a left child of his parent (or prev. parent).",
                           log_file=log_file)
                # Do not set this flag because the childs of this node have to be analyzed.
                # node['parameters']['object_not_correspond'] = 1

                if child_by_left:
                    del df[nodename]['parameters']['child_by_left']
                if old_left:
                    del df[nodename]['parameters']['old_left']
                break

            if field_deleted_from_right_select is None:  # There are multiple fields to deprecate but 1 of them is here
                field_deleted_from_right_select = False

            # For now, do not allow delete a field if it is a left or right key of an augment node.
            # Verification about left_key field is now moved to where we check for old_left or child_by_left.
            for field in fields_to_del:
                # if field in node['parameters']['left_key']:
                #     msg = f"Can not delete the field '{field}' from {{{nodename}}} node. " \
                #           f"The field appears as a `left_key` in an agument node. " \
                #           f"For now, we don't support key field deletion from an augment node."
                #     raise KeyFieldDeletionException(msg)
                if field in node['parameters']['right_key']:
                    msg = f"Can not delete the field '<strong><code>{field}</strong></code>' of <code>{obj_name}</code> object from <strong><code>{{{nodename}}}</strong></code> node. " \
                          f"The field appears as a <code>right_key</code> in an agument node. " \
                          f"For now, we don't support key field deletion from an augment node."
                    raise KeyFieldDeletionException(msg)

            right_composed_fields = []
            right_default_fields = []
            backup_right_selects = node['parameters']['right_select'].copy()
            current_right_selects = backup_right_selects.copy()
            relationship = node['parameters']['relationship']
            removed_fields = []  # For exception raising purpose

            # The fields are a sub-set of right_select fields
            for field in fields_to_del:
                new_field = field_default[fields_to_del.index(field)]

                if field in current_right_selects:
                    field_deleted_from_right_select = True
                    right_composed_fields.append(f"{relationship}.{field}")

                    if new_field != 'x':
                        current_right_selects = [new_field if (x == field) else x for x in current_right_selects]
                        right_default_fields.append(f"{relationship}.{new_field}")
                        Logger.log(f"Replaced '{field}' by '{new_field}'", log_file=log_file)
                        Logger.log(f"Composed field '{relationship}.{new_field}' created", log_file=log_file)
                    else:
                        current_right_selects.remove(field)
                        right_default_fields.append(new_field)
                        removed_fields.append(field)
                        Logger.log(f"Removed the field '{field}'", log_file=log_file)

                        if not len(current_right_selects):
                            # right_node_nm = node['parameters']['right']
                            # right_node = df[right_node_nm]
                            #
                            # if right_node['action'] not in ['digest', 'sfdcDigest', 'edgemart']:
                            #     msg = f"Can not delete the field(s) '{', '.join(removed_fields)}' from {{{nodename}}}" \
                            #           f" augment node. Deleting them will cause the node to finish with empty right" \
                            #           f" fields."
                            #     raise LastFieldDeletionException(msg)
                            # else:
                            #     childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist,
                            #                                     action=action)
                            #     Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
                            #     break

                            # 2021-04-08: Allow aug deletion - Three lake really won't happend and doesn't matter.
                            childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist,
                                                            action=action)
                            Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
                            break

            # Builds a list of composed field reference
            if len(right_composed_fields):
                composed_field_list_from_augment.append(
                    {
                        "object": "",
                        "status": "active",
                        "source-node": nodename,
                        "fields": right_composed_fields,
                        "defaults": {key: right_default_fields for key in field_metadata['defaults']}
                    }
                )
                Logger.log(f"Adding composed fields: {',  '.join(right_composed_fields)}", log_file=log_file)

            if nodename in df.keys():  # The node shall have been deleted
                # Updates the 'right_select' only if there is at least 1 field deleted/replaced.
                if set(current_right_selects) != set(backup_right_selects):
                    df[nodename]['parameters']['right_select'] = current_right_selects
                    Logger.log("Updated right_select", log_file=log_file)

        elif action == 'update':
            # For now, do not allow delete a field if it is a left or right key of an augment node.
            for field in fields_to_del:
                if field in node['parameters']['left_key']:
                    msg = f"Can not delete the field '<strong><code>{field}<strong><code>' of <code>{obj_name}</code> object from </strong></code>{{{nodename}}}</strong></code> node. " \
                          f"The field appears as a <code>left_key</code> in an update node. " \
                          f"For now, we don't support key field deletion from an update node."
                    raise KeyFieldDeletionException(msg)
                if field in node['parameters']['right_key']:
                    msg = f"Can not delete the field '<strong><code>{field}</strong></code>' of <code>{obj_name}</code> object from <strong><code>{{{nodename}}}</strong></code> node. " \
                          f"The field appears as a <code>right_key</code> in an agument node. " \
                          f"For now, we don't support key field deletion from an update node."
                    raise KeyFieldDeletionException(msg)

            for field in fields_to_del:
                new_field = field_default[fields_to_del.index(field)]
                current_update_columns = df[nodename]['parameters']['update_columns'].copy()
                Logger.log(f"  Previous 'updated_columns': {',  '.join([key + '.' + value for key, value in current_update_columns.items()])}",
                    log_file=log_file)

                if new_field != 'x':
                    new_updated_cols = {new_field if l_cname == field else
                                        l_cname: (new_field if r_cname == field else r_cname)
                                        for l_cname, r_cname in current_update_columns.items()}
                else:
                    new_updated_cols = {l_cname: r_cname
                                        for l_cname, r_cname in current_update_columns.items()
                                        if l_cname != field and r_cname != field}

                df[nodename]['parameters']['update_columns'] = new_updated_cols
                Logger.log(f"  Updated(or not) 'update_columns': {',  '.join([key + '.' + value for key, value in new_updated_cols.items()])}",
                    log_file=log_file)

            if not len(df[nodename]['parameters']['update_columns']):
                childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist, action=action)
                Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
                break

        elif action == 'filter':
            for field in fields_to_del:
                for filter_type in ['filter', 'saqlFilter']:
                    if filter_type not in node['parameters'].keys():
                        continue

                    filter_sentence = node['parameters'][filter_type]
                    new_field = field_default[fields_to_del.index(field)]

                    if filter_type == 'filter':
                        # The field is in filter sentence and the replacement does exist
                        if field == filter_sentence and new_field != 'x':
                            # Replace the filter field
                            filter_sentence = filter_sentence.replace(field, new_field)

                            # Overrides the filter field expression
                            df[nodename]['parameters'][filter_type] = filter_sentence
                            Logger.log(f"Replaced '{field}' by '{new_field}'", log_file=log_file)

                        # The field is in the filter sentence but the replacement is not specified
                        elif field == filter_sentence and new_field == 'x':
                            # 21-03-15: Decided with Subbarao to delete (at first instance) the entire node.
                            # Deletes the node if possible
                            childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist,
                                                            action=action)
                            Logger.log(f"Removed the '{action}' node '{nodename}'.", log_file=log_file)
                            # Break, since the node was deleted, no more field to analyze from this
                            break
                    else:
                        # The field is in filter sentence and the replacement does exist
                        if f"'{field}'" in filter_sentence and new_field != 'x':
                            # Replaces all the occurrence of the field
                            filter_sentence = re.sub(f"'{field}'", f"'{new_field}'", filter_sentence)

                            # Overrides the filter expression
                            df[nodename]['parameters'][filter_type] = filter_sentence
                            Logger.log(f"Replaced '{field}' by '{new_field}'", log_file=log_file)

                        # The field is in the filter sentence but the replacement is not specified
                        elif f"'{field}'" in filter_sentence and new_field == 'x':
                            # 21-03-15: Decided with Subbarao to delete (at first instance) the entire node.
                            # Deletes the node if possible
                            childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist,
                                                            action=action)
                            Logger.log(f"Removed the '{action}' node '{nodename}'.", log_file=log_file)
                            # Break, since the node was deleted, no more field to analyze from this
                            break

        elif action == 'dim2mea':
            deleted_new_measures = []
            default_del_measures = []

            for field in fields_to_del:
                new_field = field_default[fields_to_del.index(field)]

                if field == node['parameters']['dimension']:
                    if new_field != 'x':
                        df[nodename]['parameters']['dimension'] = new_field
                        Logger.log(f"Replaced '{field}' by '{new_field}'", log_file=log_file)
                    else:
                        deleted_new_measures.append(node['parameters']['measure'])
                        default_del_measures.append(new_field)
                        childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist, action=action)
                        Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
                        break

        elif action == 'computeExpression':
            deleted_computed_fields = []
            default_deleted_computed = []

            for field in fields_to_del:
                new_field = field_default[fields_to_del.index(field)]
                count_cp = len(df[nodename]["parameters"]["computedFields"])

                for i in range(count_cp):
                    if 'to_delete' in df[nodename]['parameters']['computedFields'][i].keys():
                        continue

                    saql_exp = df[nodename]['parameters']['computedFields'][i]['saqlExpression']
                    finder = re.compile(f'^{field}$|[\(\)]+{field}|{field}[\(\)]+|[\(\)]+{field}[\(\)]+|[\+\-\*\/\^]+{field}|{field}[\+\-\*\/\^]+|[\+\-\*\/\^]+{field}[\+\-\*\/\^]+|[\ ]+{field}|{field}[\ ]+|[\ ]+{field}[\ ]+|[\']+{field}|{field}[\']+|[\']+{field}[\']+')

                    if finder.search(saql_exp):
                        if new_field != 'x':
                            saql_exp = re.sub(f"'{field}'", f"'{new_field}'", saql_exp)
                            df[nodename]['parameters']['computedFields'][i]['saqlExpression'] = saql_exp
                            Logger.log(f"Replaced '{field}' by '{new_field}' in saqlExpression", log_file=log_file)
                        else:
                            deleted_computed_fields.append(df[nodename]['parameters']['computedFields'][i]['name'])
                            default_deleted_computed.append(new_field)
                            df[nodename]['parameters']['computedFields'][i]['to_delete'] = 1
                            Logger.log(f"'{field}' is specified to be deleted so...", log_file=log_file)
                            Logger.log(
                                f"  Marked '{df[nodename]['parameters']['computedFields'][i]['name']}' to be deleted.",
                                log_file=log_file)

                    # elif field == df[nodename]['parameters']['computedFields'][i]['name']:
                    #     deleted_computed_fields.append(df[nodename]['parameters']['computedFields'][i]['name'])
                    #     default_deleted_computed.append(new_field)
                    #     df[nodename]['parameters']['computedFields'][i]['to_delete'] = 1
                    #     Logger.log(f"'{field}' (to delete) is a computed field, so...", log_file=log_file)
                    #     Logger.log(
                    #         f"  Marked '{df[nodename]['parameters']['computedFields'][i]['name']}' to be deleted",
                    #         log_file=log_file)

                    if i + 1 >= len(node["parameters"]["computedFields"]):
                        break

            computed_fields = [f for f in df[nodename]['parameters']['computedFields']
                               if 'to_delete' not in f.keys()]
            if not len(computed_fields):
                childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist, action=action)
                Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
            else:
                # Deletes all the computedFields marked to be deleted
                for i in range(len(df[nodename]['parameters']['computedFields'])):
                    n = df[nodename]['parameters']['computedFields'][i]

                    if 'to_delete' in n.keys():
                        Logger.log(
                            f"  Deleted the computed field '{df[nodename]['parameters']['computedFields'][i]['name']}'",
                            log_file=log_file)
                        del df[nodename]['parameters']['computedFields'][i]

                    if i + 1 >= len(df[nodename]['parameters']['computedFields']):
                        break

            if len(deleted_computed_fields):
                fieldlist.append(
                    {
                        "object": "",
                        "status": "active",
                        "source-node": nodename,
                        "source-node-type": action,
                        "fields": deleted_computed_fields,
                        "defaults": {key: default_deleted_computed for key in field_metadata['defaults']}
                    }
                )
                Logger.log(f"New field to delete: {',  '.join(deleted_computed_fields)}", log_file=log_file)

        elif action == 'computeRelative':
            deleted_computed_fields = []
            default_deleted_computed = []

            for field in fields_to_del:
                msg = ""

                if field in node['parameters']['partitionBy']:
                    msg = f"Can't delete the field '{field}' from the {{{nodename}}} computeRelative node. " \
                          f"The field is used by 'partitionBy' of this node."
                elif field in [x['name'] for x in node['parameters']['orderBy']]:
                    msg = f"Can't delete the field '{field}' from the {{{nodename}}} computeRelative node. " \
                          f"The field is used by 'orderBy' of this node."
                if msg:
                    raise RelativeFieldDeletionException(msg)

                count_cp = len(node['parameters']['computedFields'])
                new_field = field_default[fields_to_del.index(field)]

                for i in range(count_cp):
                    if 'to_delete' in node['parameters']['computedFields'][i].keys():
                        continue

                    finder = re.compile(f'^{field}$|[\(\)]+{field}|{field}[\(\)]+|[\(\)]+{field}[\(\)]+|[\+\-\*\/\^]+{field}|{field}[\+\-\*\/\^]+|[\+\-\*\/\^]+{field}[\+\-\*\/\^]+|[\ ]+{field}|{field}[\ ]+|[\ ]+{field}[\ ]+|[\']+{field}|{field}[\']+|[\']+{field}[\']+')
                    cf = df[nodename]['parameters']['computedFields'][i]  # 'cf' stands for 'computed field' singular
                    is_source_field = 'sourceField' in cf['expression'] and field == cf['expression']['sourceField']
                    is_saql_exp = 'saqlExpression' in cf['expression'] and finder.search(cf['expression'][
                        'saqlExpression'])
                    # is_computed_field = field == node['parameters']['computedFields'][i]['name']

                    if new_field != 'x':
                        if is_source_field:
                            cf['expression']['sourceField'] = new_field
                            Logger.log(f"Replaced sourceField '{field}' by '{new_field}'", log_file=log_file)
                        elif is_saql_exp:
                            saql_exp = cf['expression']['saqlExpression']
                            saql_exp = saql_exp.replace(f"'{field}'", f"'{new_field}'")
                            cf['expression']['saqlExpression'] = saql_exp
                            Logger.log(f"saqlExpression replaced '{field}' by '{new_field}'", log_file=log_file)
                        # elif is_computed_field:
                        #     default_deleted_computed.append(new_field)
                        #     deleted_computed_fields.append(df[nodename]['parameters']['computedFields'][i]['name'])
                        #     df[nodename]['parameters']['computedFields'][i]['to_delete'] = 1
                        #     Logger.log(f"Marked to delete: {df[nodename]['parameters']['computedFields'][i]['name']}",
                        #                log_file=log_file)
                    else:
                        if is_source_field or is_saql_exp:  # or is_computed_field:
                            default_deleted_computed.append(new_field)
                            deleted_computed_fields.append(df[nodename]['parameters']['computedFields'][i]['name'])
                            df[nodename]['parameters']['computedFields'][i]['to_delete'] = 1
                            Logger.log(f"Marked to delete: {df[nodename]['parameters']['computedFields'][i]['name']}",
                                       log_file=log_file)

                    if i + 1 >= len(node['parameters']['computedFields']):
                        break

            computed_fields = [f for f in df[nodename]['parameters']['computedFields']
                               if 'to_delete' not in f.keys()]
            if not len(computed_fields):
                childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist, action=action)
                Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
            else:
                # Deletes all the computedFields marked to be deleted
                for i in range(len(df[nodename]['parameters']['computedFields'])):
                    n = df[nodename]['parameters']['computedFields'][i]

                    if 'to_delete' in n.keys():
                        Logger.log(f"Deleted the computed: {df[nodename]['parameters']['computedFields'][i]['name']}",
                                   log_file=log_file)
                        del df[nodename]['parameters']['computedFields'][i]

                    if i + 1 >= len(df[nodename]['parameters']['computedFields']):
                        break

            if len(deleted_computed_fields):
                fieldlist.append(
                    {
                        "object": "",
                        "status": "active",
                        "source-node": nodename,
                        "source-node-type": action,
                        "fields": deleted_computed_fields,
                        "defaults": {key: default_deleted_computed for key in field_metadata['defaults']}
                    }
                )
                Logger.log(f"New field to delete: {',  '.join(deleted_computed_fields)}", log_file=log_file)

        elif action == "sliceDataset":
            for field in fields_to_del:
                new_field = field_default[fields_to_del.index(field)]
                count_slicers = len(df[nodename]['parameters']['fields'])
                i = 0

                while i < count_slicers:
                    slicer = df[nodename]['parameters']['fields'][i]

                    if field != slicer['name']:
                        i += 1
                        continue

                    if new_field != "x":
                        slicer['name'] = new_field
                        Logger.log(f"Replaced '{field}' by '{new_field}'", log_file=log_file)
                    else:
                        Logger.log(f"Deleted the field '{df[nodename]['parameters']['fields'][i]['name']}'",
                                   log_file=log_file)
                        del df[nodename]['parameters']['fields'][i]
                        count_slicers = len(df[nodename]['parameters']['fields'])

                    i += 1

                if not len(df[nodename]['parameters']['fields']):
                    childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist, action=action)
                    Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)
                    break

        elif action == 'append':
            Logger.log(f"Skipping the node '{nodename}': This is an append node.", log_file=log_file)
            break

        elif action == 'flatten':
            self_field = node['parameters']['self_field']
            parent_field = node['parameters']['parent_field']

            generated_fields = []
            defaults_for_generated_fields = []

            for field in fields_to_del:
                new_field = field_default[fields_to_del.index(field)]

                if field in [self_field, parent_field]:
                    if new_field != 'x':
                        df[nodename]['parameters']['self_id'] = self_field if field != self_field else new_field
                        df[nodename]['parameters']['parent_id'] = parent_field if field != parent_field else new_field
                    else:
                        defaults_for_generated_fields = ['x', 'x']
                        generated_fields = [node['parameters']['multi_field'], node['parameters']['path_field']]
                        childs_of_deleted = delete_node(nodename=nodename, df=df, fieldlist=fieldlist, action=action)
                        Logger.log(f"Removed the '{action}' node '{nodename}'", log_file=log_file)

                        # If one of the fields to delete was found, whatever it is a self_id or parent_id,
                        #  the node will be deleted and no other field look-up steps is required.
                        break

            # Even though the node was deleted, it still needs to continue analyzing the children
            #  looking for the original field list plus the new set of fields (multi and path fields).
            # That's why we are appending the new fields to delete into the original `fieldlist`.
            if len(generated_fields):
                fieldlist.append({
                    'object': '',
                    'status': 'active',
                    "source-node": nodename,
                    'fields': generated_fields,
                    "defaults": {key: defaults_for_generated_fields for key in field_metadata['defaults']}
                })

        elif action == 'prediction':
            Logger.log(f"Skipping the node '{nodename}': This is a prediction node.", log_file=log_file)
            break

    # Node analysis ended.
    # Now, determines whether the algorithm shall continue the analysis with its children or stop it in this node.
    # We stop the further analysis with children when:
    #   * The node is an `augment` and right source of his parent but
    #      the field-to-delete wasn't selected in his `right_select` list.
    #   * The node is a `digest` node but the object didn't match the object of the field-to-delete.

    # if node['action'] in ['digest', 'sfdcDigest'] and 'object_not_correspond' in node['parameters'].keys():
    if 'object_not_correspond' in node['parameters'].keys():
        object_not_correspond = df[nodename]['parameters']['object_not_correspond']

        del df[nodename]['parameters']['object_not_correspond']

        if object_not_correspond == 1:
            return

    if field_deleted_from_right_select is not None and not field_deleted_from_right_select:
        return

    # At this point of the code, the algorithm determined to continue with the childrens.
    if nodename in df.keys():
        childs, _ = get_child_nodes(node_name=nodename, df=df, mark_left_childs=True)
    else:
        childs = childs_of_deleted

    if len(composed_field_list_from_augment):
        Logger.up()
        _delete_from_dataflow(df=df, fieldlist=composed_field_list_from_augment, node_list=childs,
                              df_name=df_name, parent_node=nodename, log_file=log_file)
        Logger.down()
    else:
        Logger.up()
        _delete_from_dataflow(df=df, fieldlist=fieldlist, node_list=childs,
                              df_name=df_name, parent_node=nodename, log_file=log_file)
        Logger.down()


def delete_fields_of_deleted_node(df: dict = None):
    """
    To be deprecated soon. For now it doesn't make any effect on the final result.
    Keeping temporally for compatibility.

    :param df:
    :return:
    """

    aug_with_oldright = get_nodes_by_action(df=df, action='augment')
    aug_with_oldright = [(nd_nm, node) for nd_nm, node in aug_with_oldright if 'old_right' in node['parameters'].keys()]

    for aug_node_nm, aug_node in aug_with_oldright:
        for rnode_name in RNODES_WITH_REPLACED_FIELD.keys():
            if rnode_name not in ([aug_node['parameters']['old_right']] + [aug_node['parameters']['right']]):
                continue

            replaced_fields = RNODES_WITH_REPLACED_FIELD[rnode_name]
            backup_right_selects = aug_node['parameters']['right_select'].copy()
            current_right_selects = backup_right_selects.copy()

            # Removes the field. Just using list-comprehension due to its fast performance
            for rf in replaced_fields:  # 'rf' stand for 'replaced field' singular
                if rf in current_right_selects:
                    current_right_selects.remove(rf)

            # Overrides the right_select property
            df[aug_node_nm]['parameters']['right_select'] = current_right_selects

        # Removes auxiliar key
        del df[aug_node_nm]['parameters']['old_right']

        # Removes child_by_left
        if 'child_by_left' in df[aug_node_nm]['parameters'].keys():
            del df[aug_node_nm]['parameters']['child_by_left']

    return df
