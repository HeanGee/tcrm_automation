from libs.json_libs import DATAFLOW_LIST_FILENAME
from libs.prompt_libs import progress_bar_with_time
from libs.workbench_api_libs import download_dataflow, validate_dataflow, upload_dataflow
from libs.utils import current_datetime
import os
import sys
from threading import Thread

if __name__ == '__main__':
    try:
        today = current_datetime(add_time=False)

        if not os.path.isdir(today):
            raise NotADirectoryError(f"Missing required folder: '{today}'.")

        updated_dflow_path = f"{today}/updated_dataflows"
        upload_dataflow(df_path=updated_dflow_path, env=input("Salesforce Instance Environment: ").strip())
    except Exception as e:
        raise e
